﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SynthLib;
using SynthLib.Data;
using System.Xml;
using SynthLib.MidiSampleProviders;
using SynthLib.Board;
using SynthLib.Board.Modules;
using SynthLib.Oscillators;
using SynthLib.Effects;
using System.Threading;
using SynthLib.Board.Template;

namespace SynthApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly Synth synth;

        private readonly SynthData data;
        
        private Thread updateThread;

        public MainWindow()
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-US");

            data = new SynthData();
            synth = new Synth(data)
            {
                MidiSampleProviderCreator = bt => new PolyBoard(bt, 6, data),
                BoardTemplate = new BoardTemplate()
            };
            InitializeComponent();
            updateThread = new Thread(UpdateGUI);
            updateThread.Start();
            RefreshBoard();
        }

        private void SaveBoard(object sender, RoutedEventArgs e)
        {
            synth.BoardTemplate.SaveToFile(boardName.Text, synth.Data);
        }

        private void RefreshBoard()
        {
            synth.BoardTemplate = SynthSetup.SetupBoard(data);
        }

        private void RefreshBoard(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            RefreshBoard();
        }

        private void UpdateGUI()
        {
            var updateTime = (long)1e5;
            var prevUpdate = DateTime.Now.Ticks;
            while (true)
            {
                while (DateTime.Now.Ticks - prevUpdate < updateTime) ;
                prevUpdate = DateTime.Now.Ticks;
                Dispatcher.BeginInvoke(
                new ThreadStart(() =>
                {
                    leftMax.Text = "" + synth.MaxValue.left;
                    rightMax.Text = "" + synth.MaxValue.right;
                }));
            }
        }
    }
}
