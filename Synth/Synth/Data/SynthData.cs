﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Stuff;
using Stuff.Exceptions;
using SynthLib.Board;
using SynthLib.Board.Modules;
using SynthLib.Board.Template;
using SynthLib.Effects;
using SynthLib.Oscillators;

namespace SynthLib.Data
{
    /// <summary>
    /// Holds all global synth data and settings
    /// </summary>
    public class SynthData
    {
        /// <summary>
        /// The settings loaded from settings files.
        /// </summary>
        private SettingsManager settings;

        /// <summary>
        /// Used to log errors and messages.
        /// </summary>
        public Logger Log { get; }

        /// <summary>
        /// Samples per second.
        /// </summary>
        public int SampleRate { get; }

        /// <summary>
        /// The DesiredLatency value used for WaveOut.
        /// </summary>
        public int DesiredLatency { get; }

        /// <summary>
        /// How far the pitch wheel can bend. In semitones.
        /// </summary>
        public float PitchWheelRange { get; }

        /// <summary>
        /// The root directory for data files.
        /// </summary>
        public string Root { get; }

        /// <summary>
        /// Where to find saved boards.
        /// </summary>
        public PathList SavedBoardsPaths { get; }
        /// <summary>
        /// Where to find default boards.
        /// </summary>
        public PathList DefaultBoardsPaths { get; }

        /// <summary>
        /// Where to look for module type definition files.
        /// </summary>
        public PathList ModuleTypePaths { get; }
        /// <summary>
        /// Where to look for oscillator definition files.
        /// </summary>
        public PathList OscillatorPaths { get; }
        /// <summary>
        /// Where to look for effect definition files.
        /// </summary>
        public PathList EffectPaths { get; }
        /// <summary>
        /// Where to look for signal type definition files.
        /// </summary>
        public PathList SignalTypePaths { get; }

        /// <summary>
        /// A LoaderTypes of the module types found at ModuleTypePaths.
        /// </summary>
        public LoaderTypes<Module> ModuleTypes { get; }
        /// <summary>
        /// A LoaderTypes of the oscillators found at OscillatorTypePaths.
        /// </summary>
        public LoaderTypes<Oscillator> OscillatorTypes { get; }
        /// <summary>
        /// A LoaderTypes of the effects found at EffectPaths.
        /// </summary>
        public LoaderTypes<Effect> EffectTypes { get; }

        /// <summary>
        /// The default boards. Specifically designed to be used as sub-boards.
        /// </summary>
        public IReadOnlyDictionary<string, BoardTemplate> SubBoards { get; }

        /// <summary>
        /// A list of all signal types.
        /// </summary>
        public IReadOnlyDictionary<string, SignalType> SignalTypes { get; }

        /// <summary>
        /// Loads settings and data.
        /// </summary>
        /// <param name="settingsPath">The directory with settings files.</param>
        public SynthData(string settingsPath = "Assets/Settings")
        {
            settings = SettingsLoader.LoadSettings(settingsPath);

            Root = settings.GetString("paths", "root");
            if (!File.GetAttributes(Root).HasFlag(FileAttributes.Directory))
                throw new SettingsException("paths", "key", "root path must be a directory");

            var logPaths = new PathList(settings.GetStrings("paths", "log"), Root);
            Log = new Logger(logPaths.First());
            if (logPaths.Count() > 1)
                Log.Log("loadError", "Multiple log paths. Only the first is used.");

            SampleRate = settings.GetInt("main", "sampleRate");
            DesiredLatency = settings.GetInt("main", "desiredLatency");
            PitchWheelRange = settings.GetFloat("main", "pitchWheelChange");


            SavedBoardsPaths = new PathList(settings.GetStrings("paths", "savedBoards"), Root);
            DefaultBoardsPaths = new PathList(settings.GetStrings("paths", "defaultBoards"), Root);

            ModuleTypePaths = new PathList(settings.GetStrings("paths", "moduleTypes"), Root);
            OscillatorPaths = new PathList(settings.GetStrings("paths", "oscillatorTypes"), Root);
            EffectPaths = new PathList(settings.GetStrings("paths", "effectTypes"), Root);

            SignalTypePaths = new PathList(settings.GetStrings("paths", "signalTypes"), Root);


            ModuleTypes = new LoaderTypes<Module>(ModuleTypePaths, "moduleType", "SynthLib");
            OscillatorTypes = new LoaderTypes<Oscillator>(OscillatorPaths, "oscillatorType", "SynthLib");
            EffectTypes = new LoaderTypes<Effect>(EffectPaths, "effectType", "SynthLib");

            var subBoards = new Dictionary<string, BoardTemplate>();
            foreach (var file in DefaultBoardsPaths.Files())
            {
                var element = XDocument.Load(file).Root;
                var subBoard = new BoardTemplate(element, this);
                subBoards[Path.GetFileNameWithoutExtension(file)] = subBoard;
            }
            SubBoards = subBoards;

            var signalTypes = new Dictionary<string, SignalType>();
            foreach (var file in SignalTypePaths.Files())
            {
                var element = XDocument.Load(file).Root;
                var signalType = new SignalType(element);
                signalTypes[signalType.Name] = signalType;
            }
            SignalTypes = signalTypes;
        }
    }
}
