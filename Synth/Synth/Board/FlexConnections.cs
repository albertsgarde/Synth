﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Linq;

namespace SynthLib.Board
{
    /// <summary>
    /// An implementation of Connections that holds an extendable number of connections.
    /// </summary>
    public class FlexConnections : Connections
    {
        /// <summary>
        /// The connections.
        /// </summary>
        private readonly List<Connection> connections;

        /// <inheritdoc />
        public override Connection this[int index] => connections[index];

        /// <inheritdoc />
        public override int Count => connections.Count;

        /// <summary>
        /// Standard constructor for a ConnectionsArray.
        /// </summary>
        /// <param name="size">The number of connection slots.</param>
        /// <param name="freeConnectionsStart">From which index unspecified indices will be placed. Must be smaller than or equal to size.</param>
        /// <exception cref="ArgumentException">Thrown when size is smaller than freeConnectionStart.</exception>
        public FlexConnections(int size = 0, int freeConnectionsStart = 0) : base(freeConnectionsStart)
        {
            connections = new List<Connection>(size);
            if (connections.Count < FreeConnectionsStart)
                throw new ArgumentException("Size must not be smaller than freeConnectionsStart or else there won't be room for the necessary connections.");
        }

        /// <summary>
        /// Loads a FlexConnections from an XML-element.
        /// </summary>
        /// <param name="element">The XML-element from which to load the FlexConnections.</param>
        /// <exception cref="InvalidModuleSaveElementException">Thrown when the module's XML-element is ill formed.</exception>
        public FlexConnections(XElement element) : base(element)
        {
            connections = new List<Connection>();
        }


        /// <summary>
        /// Adds a connection at the specified index.
        /// If a connection is already plugged into that index, it will be replaced and returned.
        /// If the index given is higher than the number of connection slots, sufficient slots will be added.
        /// </summary>
        /// <param name="connection">The connection to plug in.</param>
        /// <param name="index">The index to plug the connection into.</param>
        /// <returns>The replaced connection. null if there was none.</returns>
        protected override Connection AddConnection(Connection connection, int index)
        {
            while (connections.Count <= index)
                connections.Add(null);
            var prevCon = connections[index];
            connections[index] = connection;
            return prevCon;
        }

        /// <inheritdoc cref="Connections"/>
        protected override void RemoveConnection(Connection connection, int index)
        {
            if (connections[index] == connection)
                connections[index] = null;
            else
                throw new ArgumentException("Connection not in the specified index.");
        }

        /// <inheritdoc cref="ConnectionsArray"/>
        protected override bool FirstFreeConnection(out int index)
        {
            for (var i = FreeConnectionsStart; i < connections.Count; ++i)
            {
                if (connections[i] == null)
                {
                    index = i;
                    return true;
                }
            }
            Debug.Assert(connections.Count >= FreeConnectionsStart);
            index = connections.Count;
            connections.Add(null);
            return true;
        }

        /// <inheritdoc cref="ConnectionsArray"/>
        public override IEnumerator<Connection> GetEnumerator() => connections.GetEnumerator();
    }
}
