﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Stuff;
using Stuff.StuffMath.Turing;
using SynthLib.Board.Modules;
using SynthLib.Data;

namespace SynthLib.Board.Template
{
    /// <summary>
    /// Represents a module in a BoardTemplate.
    /// </summary>
    public class ModuleTemplate : ISaveable
    {
        /// <summary>
        /// The module type.
        /// </summary>
        public string Type { get; }

        /// <summary>
        /// An instance of the module type this template represents with parameters set.
        /// </summary>
        private readonly Module instance;

        /// <summary>
        /// Creates a ModuleTemplate with the given ID.
        /// </summary>
        /// <param name="instance">An instance of the module type this template represents with parameters set.</param>
        public ModuleTemplate(Module instance)
        {
            Type = instance.GetType().Name;
            this.instance = instance;
        }

        /// <summary>
        /// Loads a ModuleTemplate from an XML-element.
        /// </summary>
        /// <param name="element">The XML-element from which to load.</param>
        /// <param name="data">Information about the synth in which the module will perform.</param>
        public ModuleTemplate(XElement element, SynthData data)
        {
            Type = element.ElementValue("type");
            instance = data.ModuleTypes[Type].Instance.CreateInstance(element, data);
        }

        /// <summary>
        /// Creates an instance of the module this is a template for.
        /// </summary>
        /// <returns>The created instance.</returns>
        public Module CreateInstance() => instance.Clone();

        public XElement ToXElement(string name)
        {
            var element = new XElement(name);
            element.AddValue("type", Type);
            element.Add(instance.ToXElement("instance"));
            return element;
        }
    }
}
