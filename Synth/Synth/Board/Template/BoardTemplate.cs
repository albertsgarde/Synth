﻿using System;
using System.Collections.Generic;
using System.Linq;
using SynthLib.Board.Modules;
using System.Xml.Linq;
using Stuff;
using System.Collections;
using SynthLib.Data;
using System.IO;

namespace SynthLib.Board.Template
{
    /// <summary>
    /// A design for a board.
    /// Can be edited and saved/loaded to/from file.
    /// </summary>
    public class BoardTemplate : IEnumerable<ModuleTemplate>, ISaveable
    {
        /// <summary>
        /// The name of all modules in the save file is this followed by a number.
        /// </summary>
        public const string MODULE_SAVE_PREFIX = "savedModule";

        /// <summary>
        /// The number of the latest added module.
        /// </summary>
        private int moduleNum;

        /// <summary>
        /// All modules on the board indexed by identifier.
        /// </summary>
        private readonly CrossReferencedDictionary<int, ModuleTemplate> modules;

        /// <summary>
        /// All connections between all modules.
        /// </summary>
        private readonly List<ConnectionTemplate> connections;

        /// <summary>
        /// Loads a BoardTemplate from an XML-element.
        /// </summary>
        /// <param name="element">The XML-element from which to load the board.</param>
        /// <param name="data">Information about the synth in which the board will perform.</param>
        public BoardTemplate(XElement element, SynthData data)
        {
            modules = new CrossReferencedDictionary<int, ModuleTemplate>();
            connections = new List<ConnectionTemplate>();
            foreach (var mod in InvalidBoardTemplateSaveElementException.ModuleElements(element))
            {
                if (!int.TryParse(mod.Name.LocalName.Substring(MODULE_SAVE_PREFIX.Length), out var moduleId))
                    throw new InvalidModuleSaveElementException(mod, "Module name must be '" + MODULE_SAVE_PREFIX + "' plus an integer id.");
                modules[moduleId] = new ModuleTemplate(mod, data);
            }
            if (modules.Keys1.Max() + 1 != modules.Count())
                throw new InvalidBoardTemplateSaveElementException(element, "Maximum module ID must be one less than number of modules. Module id's missing or duplicate.");

            moduleNum = modules.Count() - 1;

            foreach(var con in InvalidBoardTemplateSaveElementException.ConnectionElements(element))
                connections.Add(new ConnectionTemplate(con));

        }

        /// <summary>
        /// Initializes an empty board template.
        /// </summary>
        public BoardTemplate()
        {
            modules = new CrossReferencedDictionary<int, ModuleTemplate>();
            connections = new List<ConnectionTemplate>();
            moduleNum = -1;
        }

        /// <summary>
        /// A connection between two modules on a BoardTemplate.
        /// Contains information about the from and to indexes, but not about the type of connections list used by the modules.
        /// </summary>
        private class ConnectionTemplate : ISaveable
        {
            /// <summary>
            /// The id of the module to read from.
            /// </summary>
            public int SourceId { get; }

            /// <summary>
            /// The index on the source module to read from.
            /// </summary>
            public int SourceIndex { get; }

            /// <summary>
            /// The id of the module to deliver the signal to.
            /// </summary>
            public int DestId { get; }

            /// <summary>
            /// The index on the destination module to deliver the signal to.
            /// </summary>
            public int DestIndex { get; }

            /// <summary>
            /// Initializes a ConnectionTemplate with the given parameters.
            /// </summary>
            /// <param name="sourceIdId">The id of the module to read from.</param>
            /// <param name="sourceIndex">The index on the source module to read from.</param>
            /// <param name="destIdId">The id of the module to deliver the signal to.</param>
            /// <param name="destIndex">The index on the destination module to deliver the signal to.</param>
            public ConnectionTemplate(int sourceIdId, int sourceIndex, int destIdId, int destIndex)
            {
                SourceId = sourceIdId;
                SourceIndex = sourceIndex;
                DestId = destIdId;
                DestIndex = destIndex;
            }

            /// <summary>
            /// Loads a ConnectionTemplate from an XML-element.
            /// </summary>
            /// <param name="element">The XML-element from which to load.</param>
            public ConnectionTemplate(XElement element)
            {
                SourceId = InvalidConnectionSaveElementException.ParseInt(element.Element("source"));
                SourceIndex = InvalidConnectionSaveElementException.ParseInt(element.Element("sourceIndex"));
                DestId = InvalidConnectionSaveElementException.ParseInt(element.Element("dest"));
                DestIndex = InvalidConnectionSaveElementException.ParseInt(element.Element("destIndex"));
            }

            /// <summary>
            /// Creates a string representation of the connection.
            /// </summary>
            /// <returns>A string representation of the connection.</returns>
            public override string ToString()
            {
                return $"{{{SourceId}[{SourceIndex}] -> {DestId}[{DestIndex}]}}";
            }

            /// <summary>
            /// Saves the connection to an XML-element.
            /// </summary>
            /// <param name="name">The name of the XML-element.</param>
            /// <returns>The created XML-element.</returns>
            public XElement ToXElement(string name)
            {
                var element = new XElement(name);
                element.AddValue("source", SourceId);
                element.AddValue("sourceIndex", SourceIndex);
                element.AddValue("dest", DestId);
                element.AddValue("destIndex", DestIndex);
                return element;
            }
        }

        /// <summary>
        /// Add a module to the board.
        /// This will (for almost all modules) have no effect on the board's output if the module is not later connected to other modules.
        /// </summary>
        /// <param name="module">The module to add.</param>
        /// <returns>The ID referring to the added module in this board template.</returns>
        public int Add(Module module) => Add(new ModuleTemplate(module));

        /// <summary>
        /// Adds a module template to the board.
        /// Copies the specified module template giving it a new ID.
        /// </summary>
        /// <param name="moduleTemplate">The module template to add.</param>
        /// <returns>The ID referring to the added module template in this board template.</returns>
        public int Add(ModuleTemplate moduleTemplate)
        {
            modules[++moduleNum] = moduleTemplate;
            return moduleNum;
        }

        /// <summary>
        /// Add a series of modules to the board.
        /// This will (for almost all modules) have no effect on the board's output if the modules are not later connected to other modules.
        /// </summary>
        /// <param name="mods">The modules to add.</param>
        public void Add(params Module[] mods)
        {
            foreach (var mod in mods)
                Add(mod);
        }

        /// <summary>
        /// Connect two modules at the specified indices.
        /// </summary>
        /// <param name="sourceId">The module that sends the signal.</param>
        /// <param name="destId">The module that receives the signal.</param>
        /// <param name="sourceIndex">The output index from which to read the signal. If -1, will find the lowest free index.</param>
        /// <param name="destIndex">The input index to which to write the signal. If -1, will find the lowest free index.</param>
        public void AddConnection(int sourceId, int destId, int sourceIndex = -1, int destIndex = -1)
        {
            if (!modules.Contains(sourceId) || !modules.Contains(destId))
                throw new ArgumentException("Before creating a connection between two modules they must first be added individually.");

            connections.Add(new ConnectionTemplate(sourceId, sourceIndex, destId, destIndex));
        }

        /// <summary>
        /// Connects all the given modules. The first to the second, the second to the third, the third to the fourth, and so one.
        /// In all cases the lowest free index is used.
        /// </summary>
        /// <param name="chainModules">The list of modules to chain together.</param>
        public void AddConnections(params int[] chainModules)
        {
            for (var i = 1; i < chainModules.Length; ++i)
                AddConnection(chainModules[i - 1], chainModules[i]);
        }

        /// <summary>
        /// Copies modules and connections from the specified board into this one.
        /// </summary>
        /// <param name="boardTemplate">The board to add.</param>
        public void AddBoard(BoardTemplate boardTemplate)
        {
            var moduleNumStart = moduleNum + 1;
            foreach (var mod in boardTemplate.modules.Keys2)
                Add(mod);
            foreach (var con in boardTemplate.connections)
                AddConnection(moduleNumStart + con.SourceId, moduleNumStart + con.DestId, con.SourceIndex, con.DestIndex);
        }

        /// <summary>
        /// Create a connection between to modules based on a ConnectionTemplate.
        /// </summary>
        /// <param name="connectionTemplate">The ConnectionTemplate to reify.</param>
        /// <param name="modules">A collection of modules that contain the two that should be connected.</param>
        private static void CreateConnection(ConnectionTemplate connectionTemplate, CrossReferencedDictionary<int, Module> modules)
        {
            if (connectionTemplate.SourceIndex == -1)
            {
                if (connectionTemplate.DestIndex == -1)
                    Connections.NewConnection(modules[connectionTemplate.SourceId], modules[connectionTemplate.DestId]);
                else
                    Connections.NewConnection(modules[connectionTemplate.SourceId], modules[connectionTemplate.DestId], connectionTemplate.DestIndex);
            }
            else
            {
                if (connectionTemplate.DestIndex == -1)
                    Connections.NewConnection(modules[connectionTemplate.SourceId], connectionTemplate.SourceIndex, modules[connectionTemplate.DestId]);
                else
                    Connections.NewConnection(modules[connectionTemplate.SourceId], connectionTemplate.SourceIndex, modules[connectionTemplate.DestId], connectionTemplate.DestIndex);
            }
        }

        /// <summary>
        /// Creates a new ModuleBoard based on this template.
        /// </summary>
        /// <param name="data">Information about the context in which the board will perform.</param>
        /// <returns>A new ModuleBoard based on this template.</returns>
        public ModuleBoard CreateInstance(SynthData data)
        {
            var instanceModules = new CrossReferencedDictionary<int, Module>();
            foreach (var (name, module) in modules)
            {
                instanceModules[name] = module.CreateInstance();
            }

            foreach (var connectionTemplate in connections)
            {
                CreateConnection(connectionTemplate, instanceModules);
            }


            var board = new ModuleBoard(instanceModules.Keys2.ToArray(), data);

            return board;
        }

        /// <inheritdoc />
        public IEnumerator<ModuleTemplate> GetEnumerator()
        {
            return modules.Keys2.GetEnumerator();
        }

        /// <inheritdoc />
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Saves the BoardTemplate to an XML-element.
        /// </summary>
        /// <param name="name">The name of the XML-element.</param>
        /// <returns>An XML-element representing the BoardTemplate</returns>
        public XElement ToXElement(string name = "boardTemplate")
        {
            var element = new XElement(name);
            
            var modulesElement = element.CreateElement("modules");
            foreach (var moduleTemplate in modules.Keys2)
                modulesElement.Add(moduleTemplate.ToXElement(MODULE_SAVE_PREFIX + modules[moduleTemplate]));

            var connectionsElement = element.CreateElement("connections");
            foreach (var connectionTemplate in connections)
                connectionsElement.Add(connectionTemplate.ToXElement("connection"));

            return element;
        }

        /// <summary>
        /// Saves this BoardTemplate to a file as an XML-element.
        /// </summary>
        /// <param name="path">The path of the file to create.</param>
        public void SaveToFile(string path)
        {
            var element = ToXElement();
            element.Save(path);
        }

        /// <summary>
        /// Saves this BoardTemplate to a file as an XML-element.
        /// </summary>
        /// <param name="name">The name of the BoardTemplate. Also the name of the file.</param>
        /// <param name="data">A SynthData from which the path will be computed.</param>
        /// <param name="preDef">Whether the template should be saved as a predefined board.</param>
        public void SaveToFile(string name, SynthData data, bool preDef = false) 
            => SaveToFile(Path.Combine((preDef ? data.DefaultBoardsPaths : data.SavedBoardsPaths).First(), name + ".xml"));

        /// <summary>
        /// Loads a board template from a file given a direct path.
        /// </summary>
        /// <param name="path">The file from which to load the template.</param>
        /// <param name="data">A SynthData used to initialize the BoardTemplate.</param>
        /// <returns>A BoardTemplate loaded from the specified file.</returns>
        public static BoardTemplate LoadFromFile(string path, SynthData data) 
            => new BoardTemplate(XDocument.Load(path).Root, data);

        /// <summary>
        /// Loads a board template from a file given a name.
        /// </summary>
        /// <param name="name">The name of the board template.</param>
        /// <param name="data">A SynthData used to find the file and to initialize the BoardTemplate.</param>
        /// <param name="preDef">Whether the board is a predefined board.</param>
        /// <returns>A board template with the specified name loaded from a file.</returns>
        public static BoardTemplate LoadTemplate(string name, SynthData data, bool preDef = false) 
            => LoadFromFile(Path.Combine((preDef ? data.DefaultBoardsPaths : data.SavedBoardsPaths).First(), name + ".xml"), data);
    }
}
