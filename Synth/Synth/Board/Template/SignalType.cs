﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Stuff;

namespace SynthLib.Board.Template
{
    /// <summary>
    /// Used at board creation time to check whether boards send the correct kinds of signals to each other.
    /// </summary>
    public class SignalType : ISaveable
    {
        /// <summary>
        /// The name of the signal type.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Creates a new SignalType with all the necessary information.
        /// </summary>
        /// <param name="name">The name of the signal type.</param>
        public SignalType(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Loads a signal type from an XML-element.
        /// </summary>
        /// <param name="element">The XML-element from which to load the signal type.</param>
        public SignalType(XElement element) : this(element.ElementValue("name"))
        {
        }

        /// <summary>
        /// Saves the signal type to an XML-element.
        /// </summary>
        /// <param name="name">The name of the XML-element.</param>
        /// <returns>The created XML-element.</returns>
        public XElement ToXElement(string name)
        {
            var element = new XElement(name);
            element.AddValue("name", Name);
            return element;
        }
    }
}
