﻿using System.Diagnostics;
using SynthLib.Board.Modules;

namespace SynthLib.Board
{
    /// <summary>
    /// A single connection between two modules.
    /// </summary>
    public class Connection
    {
        /// <summary>
        /// The module from which signal is received.
        /// </summary>
        public Module Source { get; }

        /// <summary>
        /// The index on the source module from which signal is received.
        /// </summary>
        public int SourceIndex { get; }

        /// <summary>
        /// The module to which signal is delivered.
        /// </summary>
        public Module Destination { get; }

        /// <summary>
        /// The index on the destination module to which signal is delivered.
        /// </summary>
        public int DestinationIndex { get; }

        /// <summary>
        /// Creates an active connection between two modules.
        /// </summary>
        /// <param name="sourceModule">The module from which signal is received.</param>
        /// <param name="sourceOutputIndex">The index on the source module from which signal is received.</param>
        /// <param name="destModule">The module to which signal is delivered.</param>
        /// <param name="destInputIndex">The index on the destination module to which signal is delivered.</param>
        public Connection(Module sourceModule, int sourceOutputIndex, Module destModule, int destInputIndex)
        {
            Source = sourceModule;
            SourceIndex = sourceOutputIndex;
            Destination = destModule;
            DestinationIndex = destInputIndex;
        }

        /// <summary>
        /// Asserts that both source and destination have this connection in the correct indices.
        /// </summary>
        public void Validate()
        {
            Debug.Assert(Source.Outputs[SourceIndex] == this, "Connection invalid between modules of types " + Source.GetType().Name + " and " + Destination.GetType().Name + ".");
            Debug.Assert(Destination.Inputs[DestinationIndex] == this, "Connection invalid between modules of types " + Source.GetType().Name + " and " + Destination.GetType().Name + ".");
        }

        /// <summary>
        /// Removes this connection from both source and destination.
        /// </summary>
        public void Destroy()
        {
            Connections.Destroy(this);
        }
    }
}
