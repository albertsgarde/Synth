﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SynthLib.Board.Modules;

using Stuff;
using NAudio.Midi;
using SynthLib.Data;
using Stuff.Music;

namespace SynthLib.Board
{
    public class ModuleBoard
    {
        /// <summary>
        /// All the modules on the board.
        /// </summary>
        private Module[] modules;

        /// <summary>
        /// The table that each sample is filled with the outputs of each module.
        /// </summary>
        private readonly InputTable inputTable;

        /// <summary>
        /// The value of all MIDI control changes.
        /// </summary>
        private readonly List<int> controllerValues;

        /// <summary>
        /// The value of all MIDI control changes.
        /// All values are initialized to 64, and the maximum is 128.
        /// </summary>
        public IReadOnlyList<int> ControllerValues => controllerValues;

        /// <summary>
        /// Values that are kept between two ticks. Can be accessed by any modules.
        /// </summary>
        private readonly Dictionary<int, float> transmissionData;

        /// <summary>
        /// The current sample of the board's input signals. Can be accessed by any modules.
        /// </summary>
        private float[] inputSignals;

        /// <summary>
        /// The value of the pitch wheel.
        /// Starts at 8192. Range is 0-16383.
        /// </summary>
        public int PitchWheel { get; private set; }

        /// <summary>
        /// The maximum pitch shift from the pitch wheel in semi-tones.
        /// </summary>
        private readonly float pitchWheelRange;

        /// <summary>
        /// The total pitch shift from the pitch wheel in semi-tones.
        /// </summary>
        private float noteOffset;

        /// <summary>
        /// The actual current frequency of the board.
        /// This is the value given to modules.
        /// </summary>
        private float frequency;

        /// <summary>
        /// Current gain of the board.
        /// Each sample is multiplied by this value.
        /// </summary>
        private float gain;

        /// <summary>
        /// The maximum absolute value of a sample.
        /// </summary>
        public (float left, float right) MaxValue { get; private set; }

        /// <summary>
        /// Time in milliseconds since last note event.
        /// </summary>
        public long Time { get; private set; }

        /// <summary>
        /// The number of samples since last note event;
        /// </summary>
        private long samples;

        /// <summary>
        /// Whether the midi note this board is bound to is currently on.
        /// </summary>
        public bool IsNoteOn { get; private set; }

        /// <summary>
        /// Which this midi note this board is bound to.
        /// </summary>
        public int Note { get; private set; }

        /// <summary>
        /// How many samples the module thinks it produces per second.
        /// Used in deciding time between samples.
        /// </summary>
        public int SampleRate { get; }

        /// <summary>
        /// Glide time should be multiplied by this.
        /// Initialized to 1.
        /// </summary>
        public float GlideModifier { get; private set; }

        /// <param name="modules">The modules on the board. Should be connected and should not be used in other boards.</param>
        /// <param name="data">Settings of the current Synth environment.</param>
        public ModuleBoard(Module[] modules, SynthData data)
        {
            SampleRate = data.SampleRate;
            pitchWheelRange = data.PitchWheelRange;

            transmissionData = new Dictionary<int, float>();
            inputSignals = new float[]{};

            this.modules = modules;
            SortModules();
            for (var i = 0; i < modules.Length; ++i)
                this.modules[i].num = i;

            controllerValues = new List<int>(128);
            for (var i = 0; i < controllerValues.Capacity; ++i)
                controllerValues.Add(64);
            PitchWheel = 8192;

            inputTable = new InputTable(this.modules);

            noteOffset = 1;

            gain = 1;

            MaxValue = (0, 0);

            Time = 0;
            samples = 0;

            GlideModifier = 1;
        }

        /// <summary>
        /// Rebinds the board to the given MIDI note and restarts the time and sample counts.
        /// </summary>
        /// <param name="note">The note to rebind to.</param>
        public void NoteOn(int note)
        {
            Note = note;
            Time = 0;
            IsNoteOn = true;
            samples = 0;
        }

        /// <summary>
        /// Handle a MIDI note release.
        /// </summary>
        public void NoteOff()
        {
            Time = 0;
            IsNoteOn = false;
            samples = 0;
        }

        /// <summary>
        /// Handle a change in pitch wheel value.
        /// </summary>
        /// <param name="pitch">The new pitch wheel value.</param>
        public void PitchWheelChange(int pitch)
        {
            PitchWheel = pitch;
        }

        /// <summary>
        /// Handle a new value for a MIDI control change.
        /// </summary>
        /// <param name="controller">The MIDI control change that changed.</param>
        /// <param name="controllerValue">The new value of the MIDI control change.</param>
        public void ControllerChange(MidiController controller, int controllerValue)
        {
            controllerValues[(int)controller] = controllerValue;
        }

        /// <summary>
        /// Sets a transmission value.
        /// This transmission value can be accessed next sample.
        /// </summary>
        /// <param name="id">The id of the transmission value.</param>
        /// <param name="value">The new value uf the transmission value.</param>
        public void TransmitValue(int id, float value)
        {
            transmissionData[id] = value;
        }

        /// <summary>
        /// Gets a transmission value.
        /// </summary>
        /// <param name="id">The id of the transmission value to get.</param>
        /// <returns>The value of the specified transmission value.</returns>
        public float ReceiveValue(int id)
        {
            if (transmissionData.ContainsKey(id))
                return transmissionData[id];
            return 0;
        }

        /// <summary>
        /// Returns value of the current sample of the input signal with the given index.
        /// </summary>
        /// <param name="index">The index of the signal to receive.</param>
        /// <returns>The value of the current sample of the input signal with the given index.</returns>
        public float InputSignal(int index)
        {
            Debug.Assert(index >= 0);
            if (index >= inputSignals.Length)
                throw new ArgumentException("Index " + index + " out of bounds. There are only " + inputSignals.Length + " input signals.");
            return inputSignals[index];
        }

        /// <summary>
        /// Sorts all modules in topological order so they only depend on modules before them.
        /// </summary>
        private void SortModules()
        {
            Validate();
            modules = modules.TopologicalSort(m => m.Inputs.Where(con => con != null).Select(con => con.Source)).ToArray();
        }

        /// <summary>
        /// Validates the state of the module network. Does nothing if not in debug.
        /// </summary>
        public void Validate()
        {
#if DEBUG
            foreach (var mod in modules)
            {
                foreach (var con in mod.Connections().Where(c => c != null))
                    con.Validate();
            }
#endif
        }

        /// <summary>
        /// Produces the next sample.
        /// </summary>
        /// <returns>The values of the next sample.</returns>
        public (float left, float right) Next(float[] signals)
        {
            inputSignals = signals;

            GlideModifier = 1;
            gain = 1;

            ++samples;
            Time = samples * 1000 / SampleRate;

            (float left, float right) result = (0f, 0f);

            inputTable.ResetInputs();
            for (var i = 0; i < modules.Length; ++i)
            {
                var curModule = inputTable.Modules[i];
                var output = curModule.Process(Time, IsNoteOn, this);
                switch (curModule.OutputType)
                {
                    case BoardOutput.None:
                        for (var j = 0; j < output.Length; ++j)
                        {
                            if (curModule.Outputs[j] != null)
                            {
                                var dest = curModule.Outputs[j];
                                inputTable[dest.Destination.num, dest.DestinationIndex] = output[j];
                            }
                        }
                        break;
                    case BoardOutput.Left:
                        result.left += output[0];
                        break;
                    case BoardOutput.Right:
                        result.right += output[0];
                        break;
                    case BoardOutput.GlideTime:
                        GlideModifier *= output[0];
                        break;
                    case BoardOutput.PitchShift:
                        noteOffset = output[0] * pitchWheelRange;
                        break;
                    case BoardOutput.Gain:
                        gain *= output[0];
                        break;
                    default:
                        throw new Exception("Unhandled board output type: " + curModule.OutputType);
                }
            }
            result.left *= gain;
            result.right *= gain;
            MaxValue = (Math.Max(MaxValue.left, Math.Abs(result.left)), Math.Max(MaxValue.right, Math.Abs(result.right)));
            return result;
        }

        /// <summary>
        /// The data structure in which the outputs of modules are placed so later modules can be given them.
        /// </summary>
        private class InputTable
        {
            /// <summary>
            /// An ordered list of the modules.
            /// </summary>
            public Module[] Modules { get; }

            private readonly int[] moduleIndexes;

            /// <summary>
            /// The data from previous modules.
            /// </summary>
            private readonly float[] inputs;
            
            public InputTable(Module[] modules)
            {
                Modules = modules;
                var numModules = modules.Length;
                var numModuleInputs = modules.Sum(module => module.Inputs.Count);
                moduleIndexes = new int[numModules + 1];
                inputs = new float[numModuleInputs];
                for (int i = 0, index = 0; i < numModules; ++i)
                {
                    moduleIndexes[i] = index;
                    modules[i].SetInputs(new ArraySegment<float>(inputs, index, modules[i].Inputs.Count));
                    index += modules[i].Inputs.Count;
                }

                moduleIndexes[numModules] = numModuleInputs;
            }

            public float this[int module, int inputIndex]
            {
                get => inputs[moduleIndexes[module] + inputIndex];
                set => inputs[moduleIndexes[module] + inputIndex] = value;
            }

            /// <summary>
            /// Resets the input table to all zero values.
            /// </summary>
            public void ResetInputs()
            {
                for (var i = 0; i < inputs.Length; ++i)
                {
                    inputs[i] = 0;
                }
            }
        }
    }
}
