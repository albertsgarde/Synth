﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace SynthLib.Board
{
    /// <summary>
    /// The exception thrown when a board template XML-element is not well formed.
    /// </summary>
    public class InvalidBoardTemplateSaveElementException : Exception
    {
        /// <summary>
        /// The XML-element at fault.
        /// </summary>
        public XElement Element { get; }

        /// <summary>
        /// Initializes a new instance of the exception with information about the ill formed XML-element.
        /// </summary>
        /// <param name="element">The ill formed XML-element.</param>
        public InvalidBoardTemplateSaveElementException(XElement element)
        {
            Element = element;
        }

        /// <summary>
        /// Initializes a new instance of the exception with information about the ill formed XML-element and a custom message.
        /// </summary>
        /// <param name="element">The ill formed XML-element.</param>
        /// <param name="message">The custom message.</param>
        public InvalidBoardTemplateSaveElementException(XElement element, string message) : base(message)
        {
            Element = element;
        }

        /// <summary>
        /// Finds the module elements in the template element and handles the situation where there are none.
        /// </summary>
        /// <param name="element">The saved board template.</param>
        /// <returns>The module elements in the board template element.</returns>
        /// <exception cref="InvalidBoardTemplateSaveElementException">Thrown when the board template element contains no 'modules' element.</exception>
        public static IEnumerable<XElement> ModuleElements(XElement element)
        {
            var moduleElements = element.Element("modules")?.Elements();
            if (moduleElements == null)
                throw new InvalidBoardTemplateSaveElementException(element, element.Name + " does not contain a 'modules' element.");
            return moduleElements;
        }

        /// <summary>
        /// Finds the connection elements in the template element and handles the situation where there are none.
        /// </summary>
        /// <param name="element">The saved board template.</param>
        /// <returns>The connection elements in the board template element.</returns>
        /// <exception cref="InvalidBoardTemplateSaveElementException">Thrown when the board template element contains no 'connections' element.</exception>
        public static IEnumerable<XElement> ConnectionElements(XElement element)
        {
            var connectionElements = element.Element("connections")?.Elements();
            if (connectionElements == null)
                throw new InvalidBoardTemplateSaveElementException(element, element.Name + " does not contain a 'connections' element.");
            return connectionElements;
        }
    }
}
