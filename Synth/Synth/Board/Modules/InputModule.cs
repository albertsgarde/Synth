﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Stuff;
using SynthLib.Data;

namespace SynthLib.Board.Modules
{
    /// <summary>
    /// Receives signals from the ModuleBoard.
    /// </summary>
    public class InputModule : Module
    {
        /// <inheritdoc />
        public override Connections Inputs { get; }

        /// <inheritdoc />
        public override Connections Outputs { get; }


        /// <inheritdoc />
        public override BoardOutput OutputType => BoardOutput.None;

        /// <summary>
        /// What signal to receive from the module board each sample.
        /// </summary>
        public int Index { get; }

        private readonly float[] output;

        /// <summary>
        /// Initializes the module to receive the signal with the specified index.
        /// </summary>
        /// <param name="index">The index of the signal this InputModule will receive.</param>
        /// <param name="outputs">The number of outputs.</param>
        public InputModule(int index, int outputs)
        {
            Inputs = new ConnectionsArray(0);
            Outputs = new ConnectionsArray(outputs);
            if (Index < 0)
                throw new ArgumentException("Index must be non-negative.");
            Index = index;

            output = new float[1];
        }

        /// <summary>
        /// Loads a InputModule from an XML-element.
        /// </summary>
        /// <param name="element">The XML-element from which to load.</param>
        public InputModule(XElement element)
        {
            Inputs = new ConnectionsArray(element.Element("inputs"));
            Outputs = new ConnectionsArray(element.Element("outputs"));
            Index = InvalidModuleSaveElementException.ParseInt(element.Element("index"));

            output = new float[1];
        }

        /// <inheritdoc />
        public override Module Clone(int sampleRate = 44100)
        {
            return new InputModule(Index, Outputs.Count);
        }

        /// <inheritdoc />
        public override Module CreateInstance(XElement element, SynthData data)
        {
            return new InputModule(element);
        }

        /// <inheritdoc />
        protected override float[] IntProcess(long time, bool noteOn,
            ModuleBoard moduleBoard)
        {
            output[0] = moduleBoard.InputSignal(Index);
            return output;
        }

        /// <inheritdoc />
        public override XElement ToXElement(string name)
        {
            var element = base.ToXElement(name);
            element.AddValue("index", Index);
            return element;
        }
    }
}
