﻿using Stuff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Diagnostics;
using SynthLib.Data;

namespace SynthLib.Board.Modules
{
    /// <summary>
    /// Applies the specified gain to each input and outputs the total to every output adjusted for output gain.
    /// </summary>
    public class Mixer : Module
    {
        public override Connections Inputs { get; }

        public override Connections Outputs { get; }

        public Gains InputGains { get; }

        public Gains OutputGains { get; }

        public override BoardOutput OutputType { get; } = BoardOutput.None;

        public Mixer()
        {
            Useable = false;
            Inputs = new ConnectionsArray(0);
            Outputs = new ConnectionsArray(0);
            InputGains = new Gains(0);
            OutputGains = new Gains(0);
        }

        public Mixer(int InputsArray, int outputs)
        {
            Inputs = new ConnectionsArray(InputsArray);
            Outputs = new ConnectionsArray(outputs);
            InputGains = new Gains(InputsArray);
            OutputGains = new Gains(outputs);
        }

        public Mixer(float[] inputGains, float[] outputGains)
        {
            Inputs = new ConnectionsArray(inputGains.Length);
            Outputs = new ConnectionsArray(outputGains.Length);
            InputGains = new Gains(inputGains);
            OutputGains = new Gains(outputGains);
        }

        private Mixer(Mixer mixer)
        {
            Inputs = new ConnectionsArray(mixer.Inputs.Count);
            Outputs = new ConnectionsArray(mixer.Outputs.Count);
            
            InputGains = mixer.InputGains;
            OutputGains = mixer.OutputGains;
        }

        private Mixer(XElement element)
        {
            Inputs = new ConnectionsArray(element.Element("InputsArray"));
            Outputs = new ConnectionsArray(element.Element("outputs"));
            InputGains = new Gains(element.Element("inputGains"));
            OutputGains = new Gains(element.Element("outputGains"));
        }

        public struct Gains : ISaveable
        {
            private readonly float[] gains;

            public float this[int i] => gains[i];

            public Gains(int inputsArraySize)
            {
                gains = new float[inputsArraySize];
                for (var i = 0; i < inputsArraySize; ++i)
                    gains[i] = 1;
            }

            public Gains(float[] gains)
            {
                this.gains = new float[gains.Length];
                gains.CopyTo(this.gains, 0);
            }

            public Gains(XElement element)
            {
                gains = new float[element.Elements().Count()];
                for (var i = 0; i < gains.Length; ++i)
                    gains[i] = InvalidModuleSaveElementException.ParseFloat(element.Element("_" + i));
            }

            public XElement ToXElement(string name)
            {
                var element = new XElement(name);
                for (var i = 0; i < gains.Length; ++i)
                    element.AddValue("_" + i, gains[i]);
                return element;
            }

            public int Length => gains.Length;
        }

        public override Module Clone(int sampleRate = 44100)
        {
            return new Mixer(this);
        }

        public override Module CreateInstance(XElement element, SynthData data)
        {
            return new Mixer(element);
        }

        protected override float[] IntProcess(long time, bool noteOn, ModuleBoard moduleBoard)
        {
            var totalInput = InputsArray.Select((t, i) => t * InputGains[i]).Sum();

            var result = new float[Outputs.Count];
            for (var i = 0; i < result.Length; ++i)
                result[i] = totalInput * OutputGains[i];
            
            return result;
        }

        public override XElement ToXElement(string name)
        {
            var element = base.ToXElement(name);
            element.Add(InputGains.ToXElement("inputGains"));
            element.Add(OutputGains.ToXElement("outputGains"));
            return element;
        }
    }
}
