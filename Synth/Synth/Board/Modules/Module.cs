﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Diagnostics.Eventing;
using System.Xml.Linq;
using Stuff;
using SynthLib.Data;

namespace SynthLib.Board.Modules
{
    /// <summary>
    /// What kind of output this module provides.
    /// </summary>
    public enum BoardOutput
    {
        /// <summary>
        /// This module's output[0] is added to the board's left output channel.
        /// </summary>
        Left = 1,
        /// <summary>
        /// This module's output[0] is added to the board's right output channel.
        /// </summary>
        Right = 2,
        /// <summary>
        /// This module's output[0] is multiplied with the board's glide channel.
        /// </summary>
        GlideTime = 3,
        /// <summary>
        /// This module's output[0] is multiplied by the board's pitch wheel range and added to the board's tone.
        /// </summary>
        PitchShift = 4,
        /// <summary>
        /// This module's output[0] is multiplied with the board's gain.
        /// </summary>
        Gain = 5,
        /// <summary>
        /// This module has no special board hook.
        /// </summary>
        None = 0
    }

    public abstract class Module : ISaveable
    {
        public abstract Connections Inputs { get; }

        public abstract Connections Outputs { get; }

        /// <summary>
        /// What kind of output this module provides.
        /// </summary>
        public abstract BoardOutput OutputType { get; }

        protected IList<float> InputsArray;

        private bool inputsSet = false;

        protected bool Useable = true;

        public float[] Process(long time, bool noteOn, ModuleBoard moduleBoard)
        {
            Debug.Assert(Useable);
            Debug.Assert(inputsSet);
            return IntProcess(time, noteOn, moduleBoard);
        }

        protected abstract float[] IntProcess(long time, bool noteOn, ModuleBoard moduleBoard);

        /// <summary>
        /// Used by module boards. Should not be touched by anything else.
        /// </summary>
        public int num;

        public virtual void UpdateFrequency(float frequency)
        {
            Debug.Assert(Useable);
        }

        public IEnumerable<Connection> Connections()
        {
            Debug.Assert(Useable);
            return Inputs.Concat(Outputs);
        }

        /// <summary>
        /// Should be called only once per module instance.
        /// </summary>
        /// <param name="inputs">A reference to the subarray where the module's inputs will be found.</param>
        public void SetInputs(ArraySegment<float> inputs)
        {
            Debug.Assert(!inputsSet);
            this.InputsArray = inputs;
            inputsSet = true;
        }

        /// <summary>
        /// The clone should be complete apart from the connections, which should be empty.
        /// </summary>
        /// <returns>A clone of the called module.</returns>
        public abstract Module Clone(int sampleRate = 44100);

        public abstract Module CreateInstance(XElement element, SynthData data);

        /// <summary>
        /// Starts the construction of an XElement that describes the module. Should be called at the start of any Modules ToXElement implementation.
        /// Saves data about connections and type, the rest should be saved by the individual module.
        /// </summary>
        public virtual XElement ToXElement(string name)
        {
            Debug.Assert(Useable);
            var element = new XElement(name);
            element.AddValue("type", GetType().Name);
            element.Add(Inputs.ToXElement("inputs"));
            element.Add(Outputs.ToXElement("outputs"));
            return element;
        }
    }
}
