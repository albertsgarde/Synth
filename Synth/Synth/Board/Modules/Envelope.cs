﻿using Stuff;
using SynthLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SynthLib.Board.Modules
{
    /// <summary>
    /// Returns a value between -1 and 0 as long as the given values are between 0 and 1.
    /// </summary>
    public class Envelope : Module
    {
        /// <summary>
        /// The sample rate of the synth in which this module will perform.
        /// </summary>
        public int SampleRate { get; }

        /// <summary>
        /// Number of milliseconds before reaching full value.
        /// </summary>
        public int Attack { get; }

        /// <summary>
        /// Number of milliseconds after reaching full value before reaching sustain value.
        /// </summary>
        public int Decay { get; }

        /// <summary>
        /// Number of milliseconds before sustain.
        /// </summary>
        private readonly int sustainStart;

        /// <summary>
        /// Sustain value as fraction of full value.
        /// </summary>
        public float Sustain { get; }

        /// <summary>
        /// Number of milliseconds for value to fade to 0 after note off.
        /// </summary>
        public int Release { get; }

        /// <summary>
        /// Change in value each sample during release.
        /// </summary>
        private readonly float releaseIncrement;

        /// <summary>
        /// All values in the on section.
        /// </summary>
        private readonly float[] onValues;

        /// <summary>
        /// The current value of the envelope.
        /// </summary>
        private float curValue;

        /// <inheritdoc />
        public override Connections Inputs { get; }

        /// <inheritdoc />
        public override Connections Outputs { get; }

        /// <inheritdoc />
        public override BoardOutput OutputType => BoardOutput.None;

        /// <summary>
        /// Returned by Next.
        /// </summary>
        private readonly float[] output;

        /// <summary>
        /// Default constructor required by Module.
        /// </summary>
        public Envelope()
        {
            Useable = false;
        }

        /// <summary>
        /// Initializes the Envelope with all stats specified.
        /// </summary>
        /// <param name="attack">Number of milliseconds before reaching full value.</param>
        /// <param name="decay">Number of milliseconds after reaching full value before reaching sustain value.</param>
        /// <param name="sustain">Sustain value as fraction of full value.</param>
        /// <param name="release">Number of milliseconds for value to fade to 0 after note off.</param>
        /// <param name="outputs">The number of outputs.</param>
        /// <param name="sampleRate">The sample rate of the synth in which this module will perform.</param>
        public Envelope(int attack, int decay, float sustain, int release, int outputs, int sampleRate = 44100)
        {
            SampleRate = sampleRate;
            Attack = attack;
            Decay = decay;
            sustainStart = Attack + Decay;
            Sustain = sustain;
            Release = release;
            releaseIncrement = -Sustain * 1000 / (Release * SampleRate);

            curValue = -1;

            Inputs = new ConnectionsArray(0);
            Outputs = new ConnectionsArray(outputs);

            onValues = CalculateOnValues();

            output = new float[Outputs.Count];
        }

        /// <summary>
        /// Initializes the envelope with no attack and no release.
        /// </summary>
        /// <param name="outputs">The number of outputs.</param>
        /// <param name="sampleRate">The sample rate of the synth in which this module will perform.</param>
        public Envelope(int outputs, int sampleRate = 44100) : this(0, 0, 1, 0, outputs, sampleRate)
        {

        }

        /// <summary>
        /// Loads the Envelope from an XML-element
        /// </summary>
        /// <param name="element">The XML-element from which to load.</param>
        /// <param name="data">Information about the synth in which the module will perform.</param>
        public Envelope(XElement element, SynthData data)
        {
            SampleRate = data.SampleRate;
            Attack = InvalidModuleSaveElementException.ParseInt(element.Element("attack"));
            Decay = InvalidModuleSaveElementException.ParseInt(element.Element("decay"));
            sustainStart = Attack + Decay;
            Sustain = InvalidModuleSaveElementException.ParseFloat(element.Element("sustain"));
            Release = InvalidModuleSaveElementException.ParseInt(element.Element("release"));
            releaseIncrement = -Sustain * 1000 / (Release * SampleRate);

            curValue = -1;

            Inputs = new ConnectionsArray(element.Element("InputsArray"));
            Outputs = new ConnectionsArray(element.Element("outputs"));

            onValues = CalculateOnValues();

            output = new float[Outputs.Count];
        }

        /// <summary>
        /// Calculates all values in the on section.
        /// </summary>
        /// <returns>The calculated values.</returns>
        private float[] CalculateOnValues()
        {
            var result = new float[sustainStart];
            for (var t = 0; t < Attack; ++t)
                result[t] = (float)t / Attack - 1;
            for (var t = Attack; t < sustainStart; ++t)
                result[t] = 0 - (t - Attack) * (1 - Sustain) / Decay;
            return result;
        }

        /// <inheritdoc />
        public override Module Clone(int sampleRate = 44100)
        {
            return new Envelope(Attack, Decay, Sustain, Release, Outputs.Count, SampleRate);
        }

        /// <inheritdoc />
        public override Module CreateInstance(XElement element, SynthData data)
        {
            return new Envelope(element, data);
        }

        /// <inheritdoc />
        protected override float[] IntProcess(long time, bool noteOn,
            ModuleBoard moduleBoard)
        {
            if (noteOn)
            {
                if (time < sustainStart)
                    curValue = onValues[time];
                else
                    curValue = Sustain - 1;
            }
            else
            {
                curValue += releaseIncrement;
                if (curValue < -1)
                    curValue = -1;
            }
            for (var i = 0; i < output.Length; ++i)
                output[i] = curValue;
            return output;
        }

        /// <inheritdoc />
        public override XElement ToXElement(string name)
        {
            var element = base.ToXElement(name);
            element.AddValue("attack", Attack);
            element.AddValue("decay", Decay);
            element.AddValue("sustain", Sustain);
            element.AddValue("release", Release);
            return element;
        }
    }
}
