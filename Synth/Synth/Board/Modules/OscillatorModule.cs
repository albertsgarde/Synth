﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SynthLib.Oscillators;
using Stuff.Music;
using Stuff;
using System.Xml.Linq;
using SynthLib.Data;

namespace SynthLib.Board.Modules
{
    /// <summary>
    /// Contains an oscillator that generates a signal based on a frequency.
    /// </summary>
    public class OscillatorModule : Module
    {
        /// <summary>
        /// The oscillator that generates the signal.
        /// </summary>
        private readonly Oscillator oscillator;

        /// <summary>
        /// Multiplies the signal.
        /// </summary>
        private readonly float gain;

        /// <summary>
        /// What the given frequency is multiplied with.
        /// </summary>
        private readonly float frequencyMultiplier;

        /// <inheritdoc />
        public override Connections Inputs { get; } // 0: Frequency, 1: Gain modifier;

        /// <inheritdoc />
        public override Connections Outputs { get; }

        /// <inheritdoc />
        public override BoardOutput OutputType { get; } = BoardOutput.None;

        /// <summary>
        /// Returned by Next.
        /// </summary>
        private readonly float[] output;

        /// <summary>
        /// Default constructor required by Module.
        /// </summary>
        public OscillatorModule()
        {
            Useable = false;
        }

        /// <summary>
        /// Initializes the module with all required information
        /// </summary>
        /// <param name="oscillator">The oscillator that generates the signal.</param>
        /// <param name="outputs">The number of outputs.</param>
        /// <param name="halfToneOffset">How many halftones to offset the given frequency by.</param>
        /// <param name="gain">Will multiply the output.</param>
        /// <param name="sampleRate">The sample rate of the Synth in which the module will perform.</param>
        public OscillatorModule(Oscillator oscillator, int outputs, float halfToneOffset = 0, float gain = 1f, int sampleRate = 44100)
        {
            this.oscillator = oscillator.Clone(sampleRate);
            frequencyMultiplier = (float) Tone.FrequencyMultiplierFromNoteOffset(halfToneOffset);
            this.gain = gain;
            Inputs = new ConnectionsArray(2, 2);
            Outputs = new ConnectionsArray(outputs);
            output = new float[outputs];
        }

        /// <summary>
        /// Constructs the OscillatorModule as a copy of another.
        /// </summary>
        /// <param name="oscMod">The OscillatorModule to copy.</param>
        /// <param name="sampleRate">The sample rate of the new oscillator.</param>
        private OscillatorModule(OscillatorModule oscMod, int sampleRate)
        {
            oscillator = oscMod.oscillator.Clone(sampleRate);
            gain = oscMod.gain;
            frequencyMultiplier = oscMod.frequencyMultiplier;
            Inputs = new ConnectionsArray(2, 2);
            Outputs = new ConnectionsArray(oscMod.Outputs.Count);
            output = new float[Outputs.Count];
        }

        /// <summary>
        /// Loads the OscillatorModule from an XML-element.
        /// </summary>
        /// <param name="element">The XML-element from which to load.</param>
        /// <param name="data">Information about the synth in which the provider will perform.</param>
        private OscillatorModule(XElement element, SynthData data)
        {
            oscillator = data.OscillatorTypes[element.Element("osc").ElementValue("type")].Instance.CreateInstance(element.Element("osc"), data);
            gain = InvalidModuleSaveElementException.ParseFloat(element.Element("gain"));
            frequencyMultiplier = InvalidModuleSaveElementException.ParseFloat(element.Element("frequencyMultiplier"));
            Inputs = new ConnectionsArray(element.Element("inputs"));
            Outputs = new ConnectionsArray(element.Element("outputs"));
            output = new float[Outputs.Count];
        }

        /// <inheritdoc />
        public override Module Clone(int sampleRate = 44100) => new OscillatorModule(this, sampleRate);

        /// <inheritdoc />
        public override Module CreateInstance(XElement element, SynthData data)
        {
            return new OscillatorModule(element, data);
        }

        /// <inheritdoc />
        protected override float[] IntProcess(long time, bool noteOn,
            ModuleBoard moduleBoard)
        {
            oscillator.Frequency = InputsArray[0] * frequencyMultiplier;
            var next = oscillator.NextValue() * gain * (InputsArray[1] + 1);
            for (var i = 0; i < output.Length; ++i)
                output[i] = next;
            return output;
        }

        /// <inheritdoc />
        public override XElement ToXElement(string name)
        {
            var element = base.ToXElement(name);
            element.Add(oscillator.ToXElement("osc"));
            element.AddValue("gain", gain);
            element.AddValue("frequencyMultiplier", frequencyMultiplier);
            return element;
        }
    }
}
