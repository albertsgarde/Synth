﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;

namespace SynthLib.Board
{
    /// <summary>
    /// An implementation of Connections that holds the connections as a fixed size array.
    /// </summary>
    public class ConnectionsArray : Connections
    {
        /// <summary>
        /// The connections.
        /// </summary>
        private readonly Connection[] connections;

        /// <summary>
        /// The number of connection slots.
        /// </summary>
        public override int Count => connections.Length;

        /// <summary>
        /// Standard constructor for a ConnectionsArray.
        /// </summary>
        /// <param name="size">The number of connection slots.</param>
        /// <param name="freeConnectionsStart">From which index unspecified indices will be placed.</param>
        public ConnectionsArray(int size, int freeConnectionsStart = 0) : base(freeConnectionsStart)
        {
            connections = new Connection[size];
            for (var i = 0; i < connections.Length; ++i)
                connections[i] = null;
        }

        /// <summary>
        /// Loads a ConnectionsArray from an XML-element.
        /// </summary>
        /// <param name="element">The XML-element from which to load the ConnectionsArray.</param>
        /// <exception cref="InvalidModuleSaveElementException">Thrown when the module's XML-element is ill formed.</exception>
        public ConnectionsArray(XElement element) : base(element)
        {
            connections = new Connection[InvalidModuleSaveElementException.ParseInt(element.Element("count"))];
            for (var i = 0; i < connections.Length; ++i)
                connections[i] = null;
        }

        /// <summary>
        /// The connection at the specified index.
        /// </summary>
        /// <param name="index">The index to look up.</param>
        /// <returns>The connection at the specified index.</returns>
        public override Connection this[int index] => connections[index];

        /// <inheritdoc />
        protected override Connection AddConnection(Connection connection, int index)
        {
            var prevCon = connections[index];
            connections[index] = connection;
            return prevCon;
        }

        /// <inheritdoc />
        protected override void RemoveConnection(Connection connection, int index)
        {
            if (connections[index] == connection)
                connections[index] = null;
            else
                throw new ArgumentException("Connection not in the specified index.");
        }

        /// <inheritdoc />
        protected override bool FirstFreeConnection(out int index)
        {
            for (var i = FreeConnectionsStart; i < connections.Length; ++i)
            {
                if (connections[i] == null)
                {
                    index = i;
                    return true;
                }
            }
            index = FreeConnectionsStart;
            return false;
        }

        /// <inheritdoc />
        public override IEnumerator<Connection> GetEnumerator() => connections.AsEnumerable().GetEnumerator();
    }
}
