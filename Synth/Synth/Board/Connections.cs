﻿using System;
using System.Collections;
using System.Collections.Generic;
using SynthLib.Board.Modules;
using Stuff;
using System.Xml.Linq;

namespace SynthLib.Board
{
    /// <summary>
    /// Represents either a modules output connections or its input connections.
    /// </summary>
    public abstract class Connections : IEnumerable<Connection>, ISaveable
    {
        /// <summary>
        /// Finds the connection plugged into the specified index.
        /// </summary>
        /// <param name="index">The index to check.</param>
        /// <returns>The connection plugged into the specified index.</returns>
        public abstract Connection this[int index]
        {
            get;
        }

        /// <summary>
        /// The number of connection slots.
        /// </summary>
        public abstract int Count { get; }

        /// <summary>
        /// Which index non-specifically assigned connections will be connected to first.
        /// </summary>
        protected int FreeConnectionsStart { get; }

        /// <summary>
        /// Sets the index to which non-specifically assigned connections will be connected to first.
        /// </summary>
        /// <param name="freeConnectionsStart">Which index non-specifically assigned connections will be connected to first.</param>
        protected Connections(int freeConnectionsStart)
        {
            FreeConnectionsStart = freeConnectionsStart;
        }

        /// <summary>
        /// Loads a Connections from an XML-element.
        /// </summary>
        /// <param name="element">The element representing the Connections.</param>
        protected Connections(XElement element)
        {
            if (int.TryParse(element.ElementValue("freeConnectionsStart"), out var freeConnectionsStart))
                FreeConnectionsStart = freeConnectionsStart;
            else
                throw new InvalidModuleSaveElementException(element);
        }

        /// <summary>
        /// Creates a new connection between two modules at the specified indexes.
        /// If the modules already have a connection at those indexes, the existing connections will be destroyed and returned.
        /// </summary>
        /// <param name="source">The module to connect from.</param>
        /// <param name="sourceIndex">Which of the source's output indexes to plug the connection in to.</param>
        /// <param name="dest">The module to connect to.</param>
        /// <param name="destIndex">Which of the destination's input indexes to plug the connection in to.</param>
        /// <returns>A tuple of first the source's old connection and then the destination's old connection. null if there were none.</returns>
        public static (Connection, Connection) NewConnection(Module source, int sourceIndex, Module dest, int destIndex)
        {
            var connection = new Connection(source, sourceIndex, dest, destIndex);
            var oldSourceConnection = source.Outputs.AddConnection(connection, sourceIndex);
            var oldDestConnection = dest.Inputs.AddConnection(connection, destIndex);
            if (oldSourceConnection != null)
                Destroy(oldSourceConnection);
            if (oldDestConnection != null)
                Destroy(oldDestConnection);
            connection.Validate();
            return (oldSourceConnection, oldDestConnection);
        }

        /// <summary>
        /// Creates a new connection between the lowest available index in the source module and the specified index in the destination module.
        /// If the destination module already has a connection in the specified index, that connection is destroyed and returned
        /// </summary>
        /// <param name="source">The module to connect from.</param>
        /// <param name="dest">The module to connect to.</param>
        /// <param name="destIndex">Which of the destinations input indexes to plug the connection in to.</param>
        /// <returns>The destination's old connection. null if there was none.</returns>
        /// <exception cref="ArgumentException">Thrown when the source module has no free connections.</exception>
        public static Connection NewConnection(Module source, Module dest, int destIndex)
        {
            if (source.Outputs.FirstFreeConnection(out var sourceIndex))
                return NewConnection(source, sourceIndex, dest, destIndex).Item2;
            else
                throw new NoFreeConnectionsException(source, "Source module has no free connections.");
        }

        /// <summary>
        /// Creates a new connection between the specified index in the source module and the lowest available index in the destination module.
        /// If the source module already has a connection in the specified index, that connection is destroyed and returned
        /// </summary>
        /// <param name="source">The module to connect from.</param>
        /// <param name="sourceIndex">Which of the sources output indexes to plug the connection in to.</param>
        /// <param name="dest">The module to connect to.</param>
        /// <returns>The source's old connection. null if there was none.</returns>
        /// <exception cref="ArgumentException">Thrown when the destination module has no free connections.</exception>
        public static Connection NewConnection(Module source, int sourceIndex, Module dest)
        {
            if (dest.Inputs.FirstFreeConnection(out var destIndex))
                return NewConnection(source, sourceIndex, dest, destIndex).Item1;
            else
                throw new NoFreeConnectionsException(dest, "Destination module has no free connections.");
        }

        /// <summary>
        /// Creates a new connection between the lowest available indexes in the specified modules.
        /// </summary>
        /// <param name="source">The module to connect from.</param>
        /// <param name="dest">The module to connect to.</param>
        /// <exception cref="ArgumentException">Thrown when either the source or the destination modules has no free connections.</exception>
        public static void NewConnection(Module source, Module dest)
        {
            if (source.Outputs.FirstFreeConnection(out var sourceIndex))
            {
                if (dest.Inputs.FirstFreeConnection(out var destIndex))
                    NewConnection(source, sourceIndex, dest, destIndex);
                else
                    throw new NoFreeConnectionsException(dest, "Destination module has no free connections.");
            }
            else
                throw new NoFreeConnectionsException(source, "Source module has no free connections.");
        }

        /// <summary>
        /// Destroys the specified connection by removing it from both the source and destination.
        /// </summary>
        /// <param name="connection">The connection to destroy</param>
        /// <exception cref="ArgumentException">Thrown when the Connection plugged into both source and destination.</exception>
        public static void Destroy(Connection connection)
        {
            connection.Source.Outputs.RemoveConnection(connection, connection.SourceIndex);
            connection.Destination.Inputs.RemoveConnection(connection, connection.DestinationIndex);
        }

        /// <summary>
        /// Plugs in the specified connection to the specified index.
        /// If there already is an in that index, that connection will be replaced and returned.
        /// </summary>
        protected abstract Connection AddConnection(Connection connection, int index);

        /// <summary>
        /// Unplugs the specified connection at the specified index.
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when the Connection isn't at the specified index.</exception>
        protected abstract void RemoveConnection(Connection connection, int index);

        /// <param name="index">The lowest index with no connection. -1 if no such index exists.</param>
        /// <returns>Whether an index with no connection exists.</returns>
        protected abstract bool FirstFreeConnection(out int index);

        /// <summary>
        /// Should enumerate all connections by index.
        /// </summary>
        /// <returns></returns>
        public abstract IEnumerator<Connection> GetEnumerator();

        /// <inheritdoc />
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        /// <summary>
        /// Constructs an XElement that describes the connections.
        /// Saves data about the number of connections and start of the free connections, the rest should be saved by the individual connections.
        /// </summary>
        /// <remarks>
        /// When implementing this method, the base method should be called at the start, and further changes should be made to that element.
        /// Note that the connections should not be saved, as they are a property of the module board, not the individual modules.
        /// </remarks>
        public virtual XElement ToXElement(string name)
        {
            var element = new XElement(name);
            element.AddValue("count", Count);
            element.AddValue("freeConnectionsStart", FreeConnectionsStart);
            return element;
        }
    }
}
