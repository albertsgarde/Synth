﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stuff.Music;
using Stuff;
using NAudio.Wave;
using SynthLib.Board;
using NAudio.Midi;
using SynthLib.Data;
using System.Threading;
using SynthLib.Board.Template;

namespace SynthLib.MidiSampleProviders
{
    /// <summary>
    /// A sample provider allowing multiple simultaneous notes to be played.
    /// </summary>
    public class PolyBoard : IMidiSampleProvider
    {
        /// <summary>
        /// The boards that together provide the signal.
        /// </summary>
        private readonly ModuleBoard[] boards;

        /// <summary>
        /// Each board's current frequency.
        /// </summary>
        private readonly float[][] frequencies;

        /// <summary>
        /// The template used to create all used boards.
        /// </summary>
        private readonly BoardTemplate boardTemplate;

        /// <summary>
        /// How many simultaneous notes are allowed.
        /// </summary>
        private readonly int voices;

        /// <inheritdoc />
        public int SampleRate { get; }

        /// <inheritdoc />
        public (float left, float right) MaxValue { get; private set; }

        /// <summary>
        /// Initializes the PolyBoard with all necessary information.
        /// </summary>
        /// <param name="boardTemplate">The template from which to create the used boards.</param>
        /// <param name="voices">How many simultaneous notes are allowed.</param>
        /// <param name="data">Information about the synth in which the provider will perform.</param>
        public PolyBoard(BoardTemplate boardTemplate, int voices, SynthData data)
        {
            SampleRate = data.SampleRate;
            MaxValue = (0, 0);
            this.boardTemplate = boardTemplate;
            this.voices = voices;
            boards = new ModuleBoard[voices];
            frequencies = new float[voices][];
            for (var i = 0; i < voices; ++i)
                frequencies[i] = new float[1];

            for (var i = 0; i < voices; ++i)
                boards[i] = boardTemplate.CreateInstance(data);
        }

        /// <inheritdoc />
        public void HandleNoteOn(int noteNumber)
        {
            if (boards.Count(mb => !mb.IsNoteOn) > 0)
            {
                var boardIndex = boards.FirstIndexOf(mb => !mb.IsNoteOn);
                boards[boardIndex].NoteOn(noteNumber);
                frequencies[boardIndex][0] = Midi.Frequencies[noteNumber];
            }
            else
            {
                var boardIndex = ContainerUtils.Count().Take(voices).MaxValue(i => boards[i].Time);
                boards[boardIndex].NoteOn(noteNumber);
                frequencies[boardIndex][0] = Midi.Frequencies[noteNumber];
            }
        }

        /// <inheritdoc />
        public void HandleNoteOff(int noteNumber)
        {
            foreach (var mb in boards)
            {
                if (mb.Note == noteNumber)
                    mb.NoteOff();
            }
        }

        /// <inheritdoc />
        public void HandlePitchWheelChange(int pitch)
        {
            foreach (var mb in boards)
                mb.PitchWheelChange(pitch);
        }

        /// <inheritdoc />
        public void HandleControlChange(MidiController controller, int controllerValue)
        {
            foreach(var mb in boards)
                mb.ControllerChange(controller, controllerValue);
        }

        /// <inheritdoc />
        public IMidiSampleProvider Clone(SynthData data)
        {
            return new PolyBoard(boardTemplate, voices, data);
        }

        /// <inheritdoc />
        public void Next(float[] buffer, int offset, int count, float gain)
        {
            for (var i = offset; i < count + offset; ++i)
                buffer[i] = 0;

            var boardsAndFrequencies = boards.Zip(frequencies);
            Parallel.ForEach(boardsAndFrequencies, () => ContainerUtils.UniformArray(0f, count), (bf, loop, subTotal) =>
            {
                var board = bf.Item1;
                var frequency = bf.Item2;
                for (var i = 0; i < count; i += 2)
                {
                    (subTotal[i], subTotal[i + 1]) = board.Next(frequency);
                }
                return subTotal;
            }, (subTotal) =>
            {
                lock (buffer)
                {
                    for (var i = 0; i < count; ++i)
                        buffer[i + offset] += subTotal[i];
                }
            });
            for (var i = offset; i < count + offset; i += 2)
            {
                MaxValue = (Math.Max(MaxValue.left, Math.Abs(buffer[i])), Math.Max(MaxValue.right, Math.Abs(buffer[i + 1])));
                buffer[i] *= gain;
                buffer[i + 1] *= gain;
            }
        }
    }
}
