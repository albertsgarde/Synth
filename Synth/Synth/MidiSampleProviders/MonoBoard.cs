﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NAudio.Wave;
using SynthLib.Board;
using Stuff.Music;
using System.Diagnostics;
using NAudio.Midi;
using SynthLib.Board.Template;
using SynthLib.Data;

namespace SynthLib.MidiSampleProviders
{
    /// <summary>
    /// A sample provider that only allows one note at a time, but has glide capabilities.
    /// </summary>
    public class MonoBoard : IMidiSampleProvider
    {
        /// <summary>
        /// The single board.
        /// </summary>
        private readonly ModuleBoard board;

        /// <summary>
        /// The template from which the board was created.
        /// </summary>
        private readonly BoardTemplate boardTemplate;

        /// <inheritdoc />
        public int SampleRate { get; }

        /// <inheritdoc />
        public (float left, float right) MaxValue { get; private set; }

        /// <summary>
        /// The glide time in milliseconds.
        /// How long it takes to change from one note to another.
        /// </summary>
        private readonly float baseGlideTime;
        /// <summary>
        /// How many samples it takes to change from one note to another.
        /// </summary>
        private readonly float glideSamples;

        /// <summary>
        /// The current frequency.
        /// </summary>
        private float frequency;

        /// <summary>
        /// The frequency of the current note, and the frequency being glided towards.
        /// </summary>
        private float destFreq;

        /// <summary>
        /// How much the frequency is adjusted each sample.
        /// </summary>
        private float baseFreqPerSample;

        /// <summary>
        /// The notes currently held down in order of press.
        /// The last note will be played.
        /// </summary>
        private readonly List<int> currentTones;

        /// <summary>
        /// Initializes the MonoBoard with all necessary information.
        /// </summary>
        /// <param name="boardTemplate">A template for the board to use.</param>
        /// <param name="glideTime">Glide time in milliseconds</param>
        /// <param name="data">Information about the synth in which the provider will perform.</param>
        public MonoBoard(BoardTemplate boardTemplate, float glideTime, SynthData data)
        {
            SampleRate = data.SampleRate;
            MaxValue = (0, 0);
            this.boardTemplate = boardTemplate;
            board = boardTemplate.CreateInstance(data);
            baseGlideTime = glideTime;
            glideSamples = (glideTime * SampleRate / 1000);
            frequency = 0;
            baseFreqPerSample = 10e8f;
            currentTones = new List<int>();
        }

        /// <inheritdoc />
        public IMidiSampleProvider Clone(SynthData data)
        {
            return new MonoBoard(boardTemplate, baseGlideTime, data);
        }

        /// <summary>
        /// Whether or not any keys are pressed.
        /// </summary>
        private bool On => currentTones.Count != 0;

        /// <inheritdoc />
        public void HandleNoteOn(int noteNumber)
        {
            if (On)
            {
                if (currentTones.Contains(noteNumber))
                    currentTones.Remove(noteNumber);
                currentTones.Add(noteNumber);
                ChangeNote(noteNumber);
            }
            else
            {
                frequency = Midi.Frequencies[noteNumber];
                board.NoteOn(noteNumber);
                currentTones.Add(noteNumber);
                baseFreqPerSample = 0;
            }
        }

        /// <inheritdoc />
        public void HandleNoteOff(int noteNumber)
        {
            currentTones.Remove(noteNumber);
            if (On)
                ChangeNote(currentTones.Last());
            else
                board.NoteOff();
        }

        /// <inheritdoc />
        public void HandlePitchWheelChange(int pitch)
        {
            board.PitchWheelChange(pitch);
        }

        /// <inheritdoc />
        public void HandleControlChange(MidiController controller, int controllerValue)
        {
            board.ControllerChange(controller, controllerValue);
        }

        /// <summary>
        /// Handles a change in note in the case where a glide is necessary.
        /// </summary>
        /// <param name="noteNumber">The note to change to.</param>
        private void ChangeNote(int noteNumber)
        {
            Debug.Assert(On);
            destFreq = Midi.Frequencies[noteNumber];
            baseFreqPerSample = (destFreq - frequency) / glideSamples;
        }

        /// <inheritdoc />
        public void Next(float[] buffer, int offset, int count, float gain)
        {
            for (var i = offset; i < count + offset; i += 2)
            {
                frequency += baseFreqPerSample / board.GlideModifier;
                if (baseFreqPerSample > 0 && frequency > destFreq || baseFreqPerSample < 0 && frequency < destFreq)
                {
                    baseFreqPerSample = 0;
                    frequency = destFreq;
                }

                var (left, right) = board.Next(new []{frequency});
                (buffer[i], buffer[i + 1]) = (left * gain, right * gain);
                MaxValue = (Math.Max(MaxValue.left, Math.Abs(buffer[i])), Math.Max(MaxValue.right, Math.Abs(buffer[i + 1])));
            }
        }
    }
}
