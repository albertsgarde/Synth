﻿// All non-rooted paths are relative to the root path.
root // Add your data path here.
// This is where boards are saved to.
savedBoards savedBoards/
// A collection of boards that are always included. Mainly subboards.
defaultBoards boards/
// Definitions of all modules.
moduleTypes moduleTypes/
// Definitions of all oscillators.
oscillatorTypes oscillatorTypes/
// Definitions of all effects.
effectTypes effectTypes/
// The place where all logs are placed. Only the first path is used.
log log/