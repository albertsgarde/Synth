When cloning use "git clone --recursive" to ensure you also get the "Stuff" submodule.

The WPF application currently has more functionality than the console. It is therefore the recommended startup project.

## Setup
### Paths
In the root directory of the project, you will find a file named paths.txt. 
This file must be copied into Synth/Synth/Assets/Settings, and the setting "root" must be set to your data directory, which is most likely the directory in the root directory named "Data".
This file is .gitignored since the root directory depends on which computer the project is on.
### SynthSetup.cs
After cloning the repository, the project won't compile until there is a SynthSetup.cs file at the path "Synth/Synth/SynthSetup.cs".\
To fix this, you can copy the SynthSetup.cs file at the top level. It should be up to date.\
Any setup of a board should happen in the Synth.Setup method defined in the .gitignored file SynthSetup.cs. 
This is to make sure your hard work designing the board doesn't get overwritten with every pull (and you don't overwrite everyone else's).
