﻿using SynthLib.Board;
using SynthLib.Board.Modules;
using SynthLib.Effects;
using SynthLib.MidiSampleProviders;
using SynthLib.Oscillators;
using SynthLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using NAudio.Midi;
using NAudio.Wave;
using SynthLib.Board.Template;

/*
This file is a working SynthSetup file.
When downloading the project, it will not work before this file or one like it is in the directory "Synth/Synth/SynthSetup.cs".
There isn't one there to start with to allow users to have different setups.
*/

namespace SynthLib
{
    public static class SynthSetup
    {
        private class ModuleManager
        {
            private readonly Dictionary<string, int> moduleIds;

            public BoardTemplate BoardTemplate { get; }

            public ModuleManager()
            {
                moduleIds = new Dictionary<string, int>();
                BoardTemplate = new BoardTemplate();
            }

            public Module this[string moduleName]
            {
                set
                {
                    if (moduleIds.ContainsKey(moduleName))
                        throw new ArgumentException("Module name " + moduleName + " already used.");
                    var moduleTemplate = new ModuleTemplate(value);
                    moduleIds[moduleName] = BoardTemplate.Add(moduleTemplate);
                }
            }

            public void AddBoard(BoardTemplate boardTemplate) => BoardTemplate.AddBoard(boardTemplate);


            public void AddConnection(string sourceName, string destName, int sourceIndex = -1, int destIndex = -1)
            {
                BoardTemplate.AddConnection(moduleIds[sourceName], moduleIds[destName], sourceIndex, destIndex);
            }

            public void AddConnections(params string[] moduleNames)
            {
                BoardTemplate.AddConnections(moduleNames.Select(name => moduleIds[name]).ToArray());
            }
    }

        public static BoardTemplate SetupBoard(SynthData data)
        {
            var mm = new ModuleManager();
             
            mm.AddBoard(data.SubBoards["glideSubBoard"]);
            
            mm.AddBoard(data.SubBoards["pitchWheelSubBoard"]);
            
            mm.AddBoard(data.SubBoards["volumeControlSubBoard"]);


            mm["in"] = new InputModule(0, 3);

            mm["env1"] = new Envelope(20, 240, 0.6f, 40, 3);

            mm["o1"] = new OscillatorModule(new SawOscillator(), 1);

            mm["o2"] = new OscillatorModule(new SawOscillator(), 1, 0.08f);

            mm["o3"] = new OscillatorModule(new SawOscillator(), 1, 11.92f);

            mm["d1"] = new Distributer(new [] { 1, 0.7f, 0.0f }, new float[] { 1 });

            //mm["sf1"] = new EffectModule(new SimpleFilter(5));
            mm["sf1"] = new EffectModule(new Filter(Filter.GenerateSincKernel(20000, 16, data.SampleRate)));

            mm["sine"] = new ConstantOscillatorModule(new SineOscillator(), 1, 40, 1);

            mm["m1"] = new Multiply();

            mm["g1"] = new EffectModule(new Boost(0.2f));

            mm["lfo2"] = new ConstantOscillatorModule(new SineOscillator(), 1, 0.5f);

            mm["p1"] = new Pan();

            mm["endLeft"] = new EndModule(false);

            mm["endRight"] = new EndModule(true);


           mm.AddConnection("in", "o1", destIndex: 0);
           mm.AddConnection("in", "o2", destIndex: 0);
           mm.AddConnection("in", "o3", destIndex: 0);

            mm.AddConnection("env1", "o1", destIndex: 1);
            mm.AddConnection("env1", "o2", destIndex: 1);
            mm.AddConnection("env1", "o3", destIndex: 1);


            mm.AddConnection("o1", "d1");
            mm.AddConnection("o2", "d1");
            mm.AddConnection("o3", "d1");

            mm.AddConnections("d1", "g1", "p1");
            
            mm.AddConnection("p1", "endLeft");
            mm.AddConnection("p1", "endRight");

            return mm.BoardTemplate;
        }

        public static Func<BoardTemplate, IMidiSampleProvider> DefaultMidiSampleProviderCreator(SynthData data)
        {
            return b => new PolyBoard(b, 12, data);
            //return b => new MonoBoard(b, 300, data);
        }

        public static int ChooseMidiInput(MidiInCapabilities[] devices)
        {
            for (var i = 0; i < devices.Length; ++i)
            {
                if (devices[i].ProductName.Contains("MPK"))
                //if (devices[i].ProductName.StartsWith("Midi"))
                    return i;
            }
            return -1;
        }

        public static int ChooseAudioDevice(WaveOutCapabilities[] devices)
        {
            for (var i = 0; i < devices.Length; ++i)
            {
                var caps = WaveOut.GetCapabilities(i);
                if (caps.ProductName.Contains("AudioBox"))
                    return i;
            }
            return -1;
        }
    }
}
